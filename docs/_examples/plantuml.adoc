.svg
[plantuml, format=svg]
....
@startuml
Bob->Alice : hello
Alice->Bob : こにちわ
@enduml
....

.png
[plantuml]
....
@startuml
Bob->Alice : hello
Alice->Bob : こにちわ
@enduml
....
