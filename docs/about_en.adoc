//po4a: entry lang
:lang: en
include::../common_parameters.adoc[]

= About

== Introduction
include::_parts/_short_introduction_{lang}.adoc[leveloffset=+2]

AGED is built on top of several open source tools, like link:https://asciidoctor.org/[Asciidoctor] and link:https://po4a.org/[po4a].
The drawback of this instead of building the tool from scratch is that it can add a loot of dependencies.
To not have all users install all this dependencies and to limit the dependency version hell AGED is packed in a link:https://cloud.docker.com/u/freke/repository/docker/freke/aged[docker container].
This reduces the installation to just have the user have to install and setup link:https://www.docker.com/[docker].

== Features

* Generate static documentation site
* Organize and navigate multiple documents
* Translations via gettext (.po files) or separate files
* Multiple versions (via git branches)
* Generate flowcharts and diagrams
* Downloadable pdf, per page or site combined to one pdf

== Requirements

* Docker (with Internet connection, for getting the latest version of AGED)
