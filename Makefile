.PHONY: all docker docs

PWD=$(shell pwd)
UID=$(shell id -u)
GID=$(shell id -g)
DOCKER_IMG=aged

DOCKER=podman
DOCKER_USER=
ifeq (, $(shell which $(DOCKER)))
	DOCKER=docker
	DOCKER_USER= -u "$(UID):$(GID)"
endif

all: docker test docs

test: unitest start_up_test start_up_test_singe_file

unitest:
	$(DOCKER) build --force-rm --no-cache --network="host" -t $(DOCKER_IMG)-test -f docker/test.dockerfile .
	$(DOCKER) run --rm --network="host" $(DOCKER_USER) -v "$(PWD)":/test $(DOCKER_IMG)-test rspec

start_up_test: docker
	echo "Test start up docs"
	rm -Rf build/_test
	mkdir -p build/_test/build
	git clone --single-branch --depth 1 -b get_started https://gitlab.com/davabe/aged.git build/_test/new-repo
	$(DOCKER) run --rm $(DOCKER_USER) -v "$(PWD)/build/_test/new-repo":/input -v "$(PWD)/build/_test/build":/output $(DOCKER_IMG)
	ls -R build/_test/build
	rm -Rf build/_test

start_up_test_singe_file: docker
	echo "Test single file"
	rm -Rf build/_test
	mkdir -p build/_test/build
	git clone --single-branch --depth 1 -b get_started https://gitlab.com/davabe/aged.git build/_test/new-repo
	$(DOCKER) run --rm $(DOCKER_USER) -v "$(PWD)/build/_test/new-repo":/input -v "$(PWD)/build/_test/build":/output $(DOCKER_IMG) doc_en.adoc
	ls -R build/_test/build
	rm -Rf build/_test

docker:
	$(DOCKER) build --force-rm --network="host" -t $(DOCKER_IMG) -f docker/aged.dockerfile .

docs:
	mkdir -p build
	$(DOCKER) run --rm --network="host" $(DOCKER_USER) -v "$(PWD)":/input -v "$(PWD)/build":/output $(DOCKER_IMG)

rebuild: clean
	mkdir -p build
	$(DOCKER) build --no-cache --force-rm --network="host" -t $(DOCKER_IMG) -f docker/aged.dockerfile .
	$(DOCKER) run --rm --network="host" $(DOCKER_USER) -v "$(PWD)":/input -v "$(PWD)/build":/output $(DOCKER_IMG)

clean:
	rm -Rf build

shell:
	mkdir -p build
	$(DOCKER) run -it --rm --network="host" -v "$(PWD)":/input -v "$(PWD)/build":/output -v "$(PWD)/lib":/aged/lib -v "$(PWD)/extensions":/aged/extensions -v "$(PWD)/bin":/aged/bin --entrypoint "/bin/bash" $(DOCKER_IMG)

publish:
	$(DOCKER) tag $(DOCKER_IMG) docker.io/freke/$(DOCKER_IMG)
	$(DOCKER) tag $(DOCKER_IMG) docker.io/freke/$(DOCKER_IMG):v0.6
	$(DOCKER) login docker.io
	$(DOCKER) push docker.io/freke/$(DOCKER_IMG)
	$(DOCKER) push docker.io/freke/$(DOCKER_IMG):v0.6