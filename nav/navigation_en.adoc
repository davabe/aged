:lang: en

// tag::menu[]
* xref:./home_{lang}#[Home]
* Documentation
** xref:./getting_started_{lang}#[Getting started]
** xref:./user_manual_{lang}#[User manual]
** xref:./extensions_{lang}#[Extensions]
* xref:./development_{lang}#[Development]
* xref:./faq_{lang}#[FAQ]
* xref:./about_{lang}#[About]
// end::menu[]
