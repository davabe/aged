require 'asciidoctor/extensions'
require 'nokogiri'
require 'securerandom'
require 'logger'

include Asciidoctor

Extensions.register do
  block TabbedBlockProcessor
end

class TabbedBlockProcessor < Extensions::BlockProcessor
  use_dsl
  LF = '\n'.freeze

  named :tabbed
  on_context :open
  parse_content_as :raw

  def process(parent, reader, attrs)
    logger = ::Asciidoctor::LoggerManager.logger

    temp = create_open_block parent.document, reader.lines, attrs

    return temp unless parent.document.attributes['backend'] =~ /^html/

    parse_content temp, reader

    tabs_id = SecureRandom.uuid

    doc = Nokogiri::HTML::Document.parse(temp.content)
    doc.search('div.listingblock').each_with_index do |code, index|
      language = code.at_css('div > pre > code').attribute('data-lang')
      logger.debug("TabbedBlockProcessor #{language}")
      id = SecureRandom.uuid
      if index.zero?
        code.before("<input class='input' name='#{tabs_id}' type='radio' id='#{id}' style='display: none;' checked>")
      else
        code.before("<input class='input' name='#{tabs_id}' type='radio' id='#{id}' style='display: none;'>")
      end
      code.before("<label class='label' for='#{id}'>#{language}</label>")
    end

    if attrs['role'].nil?
      attrs['role'] = 'tabbed'
    else
      attrs['role'] << ', tabbed'
    end

    wrapper = create_open_block parent, [], attrs
    wrapper.blocks << create_pass_block(wrapper, doc.to_html, {})
    wrapper
  end
end
