require 'asciidoctor/extensions'

Asciidoctor::Extensions.register do
  inline_macro do
    named :conum
    process do |parent, target, _attrs|
      if parent.document.attributes['backend'] =~ /^html/
        create_inline(parent, :callout, target.to_i)
      else
        create_inline(parent, :quoted, %(\(#{target}\))) # TODO: Quickfix pdf renderer will use wrong font-family and not render callout icons
      end
    end
  end
end
