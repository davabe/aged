FROM node:alpine3.13 AS compass

RUN apk update && apk add ca-certificates && update-ca-certificates && apk add openssl wget build-base libffi-dev ruby ruby-dev
RUN gem install --version '4.3.2' zurb-foundation
RUN gem install sass compass
RUN npm install clean-css-cli -g
RUN apk del build-base libffi-dev ruby-dev

WORKDIR /opt

RUN wget https://github.com/asciidoctor/asciidoctor-stylesheet-factory/archive/refs/heads/main.zip
RUN unzip main.zip

COPY sass /opt/asciidoctor-stylesheet-factory-main/sass
COPY docker/build_css.sh /opt/asciidoctor-stylesheet-factory-main/build_css.sh

WORKDIR /opt/asciidoctor-stylesheet-factory-main

FROM compass AS build_compass
RUN ./build_css.sh aged
# RUN compass compile -s compressed


FROM alpine:edge AS build_po4a
ADD https://github.com/mquinson/po4a/archive/master.zip /tmp/po4a.zip
RUN unzip /tmp/po4a.zip


FROM freke/docker_asciidoctor
RUN apk add --no-cache --update git imagemagick inotify-tools perl gettext perl-unicode-linebreak perl-yaml-tiny diffutils nodejs npm; \
    npm install terser -g; \
    rm -rf /var/cache/apk/*; \
    mkdir -p /aged/project; \
    chmod u+rw,g+rw,o+rw /aged/project; \
    mkdir /out; \
    chmod u+rw,g+rw,o+rw /out

COPY --from=build_po4a /po4a-master/lib /po4a/lib
COPY --from=build_po4a /po4a-master/po /po4a/po
COPY --from=build_po4a /po4a-master/scripts /po4a/scripts
COPY --from=build_po4a /po4a-master/share /po4a/share
COPY --from=build_po4a /po4a-master/po4a* /po4a/
COPY --from=build_compass /opt/asciidoctor-stylesheet-factory-main/aged.min.css /aged/stylesheets/
COPY --from=build_compass /opt/asciidoctor-stylesheet-factory-main/aged.css /aged/stylesheets/
ADD https://raw.githubusercontent.com/asciidoctor/asciidoctor/main/src/stylesheets/coderay-asciidoctor.css /aged/stylesheets/

ENV PERLLIB /po4a/lib
ENV PATH="/po4a:${PATH}"

ADD https://code.jquery.com/jquery-3.6.0.min.js /aged/js/jquery-3.6.0.min.js
COPY docker/elasticlunr.min.js /aged/js/elasticlunr.min.js
ADD https://cdnjs.cloudflare.com/ajax/libs/mark.js/8.11.1/jquery.mark.min.js /aged/js/jquery.mark.min.js
ADD https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/7.6.0/polyfill.min.js /aged/js/polyfill.min.js

ADD https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css /aged/css/font-awesome.min.css
ADD https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.woff2 /aged/fonts/fontawesome-webfont.woff2
ADD https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.woff /aged/fonts/fontawesome-webfont.woff
ADD https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.svg /aged/fonts/fontawesome-webfont.svg
ADD https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.ttf /aged/fonts/fontawesome-webfont.ttf

COPY docinfo/ /aged/docinfo/
COPY extensions/ /aged/extensions/
COPY images/gitbranch.svg /aged/images/
COPY images/favicon /aged/images/favicon
COPY lib/ /aged/lib/
COPY package.json /aged/package.json
COPY bin/aged.rb /aged/bin/aged.rb
COPY docker/watcher.sh /watcher.sh

RUN chmod +x /aged/lib/build-index.js; \
    chmod +r /aged/js/jquery-3.6.0.min.js; \
    chmod +r /aged/js/jquery.mark.min.js; \
    chmod +r /aged/js/polyfill.min.js; \
    chmod +r /aged/js/elasticlunr.min.js; \
    chmod +r /aged/css/font-awesome.min.css; \
    chmod +r /aged/fonts/fontawesome-webfont.woff2; \
    chmod +r /aged/fonts/fontawesome-webfont.woff; \
    chmod +r /aged/fonts/fontawesome-webfont.svg; \
    chmod +r /aged/fonts/fontawesome-webfont.ttf; \
    chmod -R +r /aged/stylesheets/; \
    mkdir -p /aged/.asciidoctor; \
    chmod -R u+rw,g+rw,o+rw /aged/.asciidoctor; \
    chmod -R u+rw,g+rw,o+rw /aged/images/

RUN terser /aged/lib/elasticlunr_lang/lunr.multi.js --compress --output /aged/lib/elasticlunr_lang/lunr.multi.min.js; \
    terser /aged/lib/elasticlunr_lang/lunr.stemmer.support.js --compress --output /aged/lib/elasticlunr_lang/lunr.stemmer.support.min.js; \
    terser /aged/lib/elasticlunr_lang/lunr.ja.js --compress --output /aged/lib/elasticlunr_lang/lunr.ja.min.js; \
    terser /aged/lib/elasticlunr_lang/lunr.sv.js --compress --output /aged/lib/elasticlunr_lang/lunr.sv.min.js; \
    terser /aged/lib/elasticlunr_lang/lunr.zh.js --compress --output /aged/lib/elasticlunr_lang/lunr.zh.min.js

VOLUME /input
VOLUME /output

WORKDIR /aged

RUN npm i elasticlunr
RUN npm link

ENTRYPOINT ["ruby", "-I", "/aged/lib/", "bin/aged.rb"]
CMD []
