mkdir build
docker build --no-cache --force-rm --pull --network="host" -f docker/aged.dockerfile -t aged .
set mypath=%cd%
docker run --rm --network="host" -v %mypath%:/input -v %mypath%\build:/output aged
