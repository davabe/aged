require 'logger'

module Aged
  class Gettext
    attr_accessor :working_dir
    attr_accessor :email
    attr_accessor :translation_dir
    attr_accessor :min_level

    def initialize(translations_files, cfg_file, executer, translation_dir:, project_name:, rev:, min_level: 80)
      @cfg_file = cfg_file
      @translations_files = translations_files
      @working_dir = './'
      @project_name = project_name
      @rev = rev
      @executer = executer
      @translation_dir = translation_dir
      @min_level = min_level
    end

    def translate
      ::Aged.logger.debug('translate')
      File.open(@cfg_file, 'w') { |f| f.write creat_cfg_file }

      cmd = "cd #{@working_dir}; po4a --previous -v --package-name '#{@project_name}' --package-version '#{@rev}' '#{@cfg_file}'"
      @output = @executer.call(cmd)
      get_stats
    end

    def save_stats(filename)
      File.open(filename, 'w') do |f|
        f.write "file\ttranslated\tfuzzy\tuntranslated\n"
        @po_stats.each do |stat|
          f.write "#{stat['file']}\t#{stat['translated']}\t#{stat['fuzzy']}\t#{stat['untranslated']}\n"
        end
      end
    end

    def save_translation_status(filename)
      File.open(filename, 'w') do |f|
        f.write "file\tpart\ttranslated\ttotal\n"

        @discarded.each do |discard|
          f.write "#{discard['file']}\t#{discard['part']}\t#{discard['translated']}\t#{discard['total'] || discard['translated']}\n"
        end

        @translations.each do |translation|
          f.write "#{translation['file']}\t#{translation['part']}\t#{translation['translated']}\t#{translation['total'] || translation['translated']}\n"
        end
      end
    end

    private
    def get_stats()
      pot_stats = @output.match(/\/(?<file>[^\/]*\.pot).*\((?<entries>\d+) entries\)/m)

      ::Aged.logger.debug("pot_stats: #{pot_stats['file']} entries: #{pot_stats['entries']}")

      @po_stats = []
      @translations = []
      @discarded = []

      @output.each_line do |line|
        po_stat = line.match(%r{\/(?<file>[^\/]+\.po):\s*(?<translated>\d+)\s+translated messages,\s+(?<fuzzy>\d+)\s+fuzzy translations,\s*(?<untranslated>\d+)\s+untranslated messages})
        translation = line.match(%r{(?<file>[^\/]+\.adoc) is\s+(?<part>\d+\.?\d+)%\s+translated \((?<translated>\d+)(:?\s+of\s+(?<total>\d+))?\s+strings\)})
        discard = line.match(%r{Discard.*\/(?<file>[^\/]+\.adoc)\s+\((?<translated>\d+)\s+of\s+(?<total>\d+)\s+strings; only\s+(?<part>\d+\.?\d*)%\s+translated; need\s+(?<limit>\d+)%\)})
        @po_stats << po_stat unless po_stat.nil?
        @translations << translation unless translation.nil?
        @discarded << discard unless discard.nil?
      end
    end

    def creat_cfg_file
      ::Aged.logger.debug('creat_cfg_file')
      cnf_file = "[po_directory] #{@translation_dir}\n"
      cnf_file += "[po4a_alias:aged] asciidoc opt:\"-k #{@min_level}\"\n\n"
      template = lambda do |src_file|
        target_file = src_file.gsub(/^(.*)_[a-z]{2}\.adoc$/, '\1_$lang.adoc')
        "[type:aged] #{src_file} $lang:#{target_file} add_$lang:?#{File.join(translation_dir, '$lang.add')}\n"
      end

      @translations_files.each { |f| cnf_file += template.call(f) }
      cnf_file
    end
  end
end
