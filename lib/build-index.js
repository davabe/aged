#!/usr/bin/env node

'use strict';

// Script includes
const fs = require('fs');
const el = require('elasticlunr');
const jsdom = require('jsdom');
const recursive = require('recursive-readdir');
const crypto = require('crypto');
const optionParser = require('command-line-args');
const path = require('path');

const VERSION = '0.1';
const DESCRIPTION = 'Script to build the "elasticlunr" search index.'
const SCRIPT_NAME = path.basename(process.argv[1]);

/**
 * Calculates the md5 hash of a string and returns the
 * result as an integer.
 */
function hashString(str) {
    var md5 = crypto.createHash('md5');
    var hex_digest = md5.update(str).digest('hex');
    // Convert to integer
    return parseInt(hex_digest.slice(0, 8), 16);
}

/**
 * Sanitises the HTML within a dom node
 * by removing all script and style tags.
 */
function sanitiseHtml(node) {
    for (var child of node.childNodes)
    {
        if (child.tagName && child.tagName.match(/script|style/i))
        {
            node.removeChild(child);
        }
    }
}

/**
 * Truncates a string to a maximum number of words.
 */
function truncateWords(str, words) {
    return str.split(/\b/).slice(0, words * 2).join('');
}

function escapeHtml(unsafe) {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
 }

 var tagBody = '(?:[^"\'>]|"[^"]*"|\'[^\']*\')*';

 var tagOrComment = new RegExp(
     '<(?:'
     // Comment body.
     + '!--(?:(?:-*[^->])*--+|-?)'
     // Special "raw text" elements whose content should be elided.
     + '|script\\b' + tagBody + '>[\\s\\S]*?</script\\s*'
     + '|style\\b' + tagBody + '>[\\s\\S]*?</style\\s*'
     // Regular name
     + '|/?[a-z]'
     + tagBody
     + ')>',
     'gi');

function removeTags(html) {
  var oldHtml;
  do {
    oldHtml = html;
    html = html.replace(tagOrComment, '');
  } while (html !== oldHtml);
  return html.replace(/</g, '&lt;');
}

function add_index(index, file, page, meta_title, article){
  if (meta_title && article)
  {
      sanitiseHtml(article);

      if (options.verbose)
      {
          console.log("Indexing: " + file);
      }

      file = file.replace(new RegExp('^' + options['index-dir'] + '\/?'), '');
      var title = meta_title.textContent;
      if(page) {
        title = page.textContent + " > " + title
      }
      var doc = {
          title: title,
          body: removeTags(article.outerHTML),
          description: truncateWords(escapeHtml(article.textContent), 50),
          url: file + "#" + meta_title.id,
          id: hashString(file + "#" + meta_title.id)
      };
      index.addDoc(doc);
  }
  return index
}

function recFindByExt(base,ext,files,result)
{
  if (!fs.existsSync(base)) {
    return []
  }
  files = files || fs.readdirSync(base)
  result = result || []

  files.forEach(
    function (file) {
      var newbase = path.join(base,file)
      if ( fs.statSync(newbase).isDirectory() )
      {
        result = recFindByExt(newbase,ext,fs.readdirSync(newbase),result)
      }
      else
      {
        if ( file.substr(-1*(ext.length+1)) == '.' + ext )
        {
          result.push(newbase)
        }
      }
    }
  )
  return result
}

const optionParams = [
    {name: 'index-dir', alias: 'd', type: String, defaultOption: true},
    {name: 'ignore', alias: 'i', type: String, multiple: true},
    {name: 'output', alias: 'o', type: String, defaultValue: './index.json'},
    {name: 'language', alias: 'l', type: String},
    {name: 'version', alias: 'v', type: Boolean},
    {name: 'verbose', alias: 'V', type: Boolean},
    {name: 'help', alias: 'h', type: Boolean}
];

const options = optionParser(optionParams);

if (options.help)
{
    console.log(SCRIPT_NAME + ' - ' + DESCRIPTION + '\n');
    console.log(
        'Usage: ' + SCRIPT_NAME + ' INDEX-DIR [--ignore FILE1 FILE2 --output OUT-FILE --verbose]'
    );
}
else if (options.version)
{
    console.log(process.argv[0] + ' - v' + VERSION);
}
else
{
    // Ignore non-html files.
    var ignore = ['*.pdf', '*.js', '*.css', '*.xml', '*.jpg', '*.JPG', '*.png', '*.PNG', '*.svg'];

    // Create the index schema.
    el.tokenizer.setSeperator(/[\s\-\.\\\/:,;]+/)

    var index = el(function () {
        this.addField('title'),
        this.addField('body'),
        this.addField('description'),
        this.addField('url'),
        this.setRef('id')

        if (options.language && options.language != "en")
        {
          var ext_file_list = recFindByExt('/aged/project/locale',options.language+'.min.js')
          if(ext_file_list.length == 0){
            ext_file_list = recFindByExt('/aged/project/locale',options.language+'.js')
          }
          if(ext_file_list.length == 0){
            ext_file_list = recFindByExt('/aged/lib/elasticlunr_lang',options.language+'.min.js')
          }
          if(ext_file_list.length == 0){
            ext_file_list = recFindByExt('/aged/lib/elasticlunr_lang',options.language+'.js')
          }
          if(ext_file_list.length == 0){
            throw 'elasticlunr_lang file ' + options.language + ' not found';
          }

          require('./elasticlunr_lang/lunr.stemmer.support.js')(el);
          require(ext_file_list[0])(el);
          require('./elasticlunr_lang/lunr.multi.js')(el);
          this.use(el.multiLanguage('en', options.language));
        }
    });

    if (options.ignore)
    {
        ignore = ignore.concat(options.ignore);
    }

    var lang = "en"
    if (options.language)
    {
        lang = options.language
    }

    console.log("Building search index for " + lang);

    var re = new RegExp("_"+lang+"\.html$");

    recursive(options['index-dir'], ignore, function (err, files) {
        if (err)
        {
            console.log(err);
            return;
        }
        for (var file of files)
        {
            if(!re.test(file)){
              if (options.verbose)
              {
                console.log("Skipping: " + file);
              }
              continue;
            }
            var html = fs.readFileSync(file, 'utf-8');
            // Parse the DOM.
            const { JSDOM } = jsdom;
            const { window } = new JSDOM();
            const { document } = (new JSDOM(html)).window;
            global.document = document;

            var meta_title = document.querySelector('h1');
            var page = meta_title
            var article = document.querySelector('#preamble');
            index = add_index(index, file, null, meta_title, article);

            var sect = document.querySelectorAll('.sect1');
            for( var i = 0; i < sect.length; ++i ){
              var meta_title = sect[i].querySelector('h2');
              var divs = sect[i].querySelectorAll('.sectionbody > *:not(.sect2)')
              var article = document.createElement("section");
              for( var j = 0; j < divs.length; ++j ){
                article.appendChild(divs[j])
              }
              index = add_index(index, file, page, meta_title, article);
            }
            for(var k=2; k<9; ++k){
              var sects = document.querySelectorAll('.sect'+ k)
              for (var sec = 0; sec < sects.length; ++sec) {
                var meta_title = sects[sec].querySelector('h' + (k+1));
                var article = document.createElement("section");
                var sect = sects[sec].querySelectorAll('.sect'+k+' > *:not(.sect'+(k+1)+')')
                for( var l = 0; l < sect.length; ++l ){
                  article.appendChild(sect[l])
                }
                index = add_index(index, file, page, meta_title, article);
              }
            }
        }

        // Serialise and write the index.
        var out = index.toJSON();

        // Remove the body field from the documentStore to decrease the size of the index.
        for (var id in out.documentStore.docs)
        {
            delete out.documentStore.docs[id].body;
        }

        if (options.verbose)
        {
            console.log("Serialising to: " + options.output)
        }
        fs.mkdir(path.dirname(options.output), { recursive: true }, (err) => {
            if (err) throw err;
        });
        fs.writeFileSync(options.output, "search_index = "+JSON.stringify(out), 'utf-8');

        if (options.verbose)
        {
            console.log('done');
        }
        process.exit();
    });
}
